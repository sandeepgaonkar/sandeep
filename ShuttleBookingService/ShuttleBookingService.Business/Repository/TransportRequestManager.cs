﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ShuttleBookingService.DataAccess.CommandExecuter;
using ShuttleBookingService.DataAccess.Queries;
using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public class TransportRequestManager : ITransportRequestManager
    {
       // private readonly IConfiguration _configuration;
        private readonly ICommandText _commandText;
        private readonly IExecuters _executers;
        public TransportRequestManager(ICommandText commandText, IExecuters executers)
        {
            _commandText = commandText;
            //_configuration = configuration;
            _executers = executers;
        }
        public List<TransportRequest> GetAllTransportRequests()
        {
            var query = _executers.ExecuteCommand(
                   conn => conn.Query<TransportRequest>(_commandText.GetAllTransportRequests)).ToList();
            return query;
        }
        public TransportRequest AddTransportRequest(TransportRequest entity)
        {
            try
            {
                var _requestId = _executers.ExecuteCommand(
                  conn => conn.Query<TransportRequest>(_commandText.AddTransportRequest, new
                  {
                      employee_id = entity.employee_id,
                      first_name = entity.first_name,
                      last_name = entity.last_name,
                      user_name = entity.user_name,
                      sex = entity.sex,
                      contact_phone = entity.contact_phone,
                      project_account = entity.project_account,
                      email = entity.email,
                      request_reason = entity.request_reason,
                      pickup_location_id = entity.pickup_location_id,
                      to_location = entity.to_location,
                      to_lattitude = entity.to_lattitude,
                      to_longitude = entity.to_longitude,
                      pin_code = entity.pin_code,
                      no_of_passengers = entity.no_of_passengers,
                      status = entity.status,
                      request_time = entity.request_time,
                      created_on = entity.created_on,
                      cancelled_on = entity.cancelled_on,
                      vendor_id = entity.vendor_id,
                      approved_by = entity.approved_by,
                      approved_on = entity.approved_on,
                      approved_by_dm = entity.approved_by_dm,
                      approved_by_dm_on = entity.approved_by_dm_on,
                      approval_reason = entity.approval_reason,
                      total_expense = entity.total_expense
                  })).Single();
                
                return _requestId;

            }
            catch (Exception Ex)
            {
                throw;
            }
            
        }
    }
}
