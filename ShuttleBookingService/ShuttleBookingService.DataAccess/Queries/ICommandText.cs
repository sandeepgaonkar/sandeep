﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.DataAccess.Queries
{
    public interface ICommandText
    {
        string GetAllTransportRequests { get; }
        string AddTransportRequest { get; }
    }
}
