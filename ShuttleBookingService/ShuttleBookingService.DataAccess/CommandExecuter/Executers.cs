﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using ShuttleBookingService.DataAccess.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ShuttleBookingService.DataAccess.CommandExecuter
{
    public class Executers : IExecuters
    {
        private readonly IDbConnection _dbConnection;

        public Executers(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }
        
        public void ExecuteCommand(Action<NpgsqlConnection> task)
        {
            using (var conn = new NpgsqlConnection(_dbConnection.ConnectionString))
            {
                conn.Open();
                task(conn);
            }
        }

        public T ExecuteCommand<T>(Func<NpgsqlConnection, T> task)
        {
            using (var conn = new NpgsqlConnection(_dbConnection.ConnectionString))
            {
                    conn.Open();
                    return task(conn);

            }
        }
    }
}
