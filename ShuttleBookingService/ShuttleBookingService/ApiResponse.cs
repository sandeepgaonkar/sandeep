﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService
{
    public class ApiResponse
    {
        public string message { get; set; }
        public object status { get; set; }
        public object data { get; set; }
    }
}
