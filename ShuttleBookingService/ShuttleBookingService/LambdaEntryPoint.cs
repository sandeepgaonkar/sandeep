using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;

namespace ShuttleBookingService
{
    /// <summary>
    /// This class extends from APIGatewayProxyFunction which contains the method FunctionHandlerAsync which is the
    /// actual Lambda function entry point. The Lambda handler field should be set to
    ///
    /// CAP.GlobalFormularyApplication.Service::CAP.GlobalFormularyApplication.Service.LambdaEntryPoint::FunctionHandlerAsync
    /// </summary>
    public class LambdaEntryPoint : Amazon.Lambda.AspNetCoreServer.APIGatewayProxyFunction
    {
      /// <summary>
      /// Initilization
      /// </summary>
      /// <param name="builder"> Init </param>
        protected override void Init(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services => services.AddAutofac()).UseStartup<Startup>();
        }
    }
}
