﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace ShuttleBookingService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]  
    public class UserController : ControllerBase
    {      
        [HttpGet]
        [Route("login")] 
        public async Task<string> GetAsync(string userName, string password)
        {
            HttpClient client = new HttpClient();
           // var byteArray = Encoding.ASCII.GetBytes("admin:Admin123");
           // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));          
            var baseURL = "http://arc-staging.emids.com:121/api/";
            var functionPath = "UserAuth?userName="+userName+"&password="+password;

            HttpResponseMessage response = await client.GetAsync(baseURL + functionPath);
            return await response.Content.ReadAsStringAsync();
           //return "gotresponse";
                 
         }       
    }
}
