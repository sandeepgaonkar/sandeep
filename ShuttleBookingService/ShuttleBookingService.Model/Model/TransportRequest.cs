﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Model.Model
{
    public class TransportRequest
    {
        public int request_id { get; set; }
        public int employee_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string user_name { get; set; }
        public string sex { get; set; }
        public string contact_phone { get; set; }
        public string project_account { get; set; }
        public string email { get; set; }
        public string request_reason { get; set; }
        public int pickup_location_id { get; set; }
        public string to_location { get; set; }
        public string to_lattitude { get; set; }
        public string to_longitude { get; set; }
        public string pin_code { get; set; }
        public int no_of_passengers { get; set; }
        public string status { get; set; }
        public string request_time { get; set; }
        public DateTime created_on { get; set; }
        public DateTime cancelled_on { get; set; }
        public int vendor_id { get; set; }
        public string approved_by { get; set; }
        public DateTime approved_on { get; set; }
        public string approved_by_dm { get; set; }
        public DateTime approved_by_dm_on { get; set; }
        public string approval_reason { get; set; }
        public string total_expense { get; set; }
    }
}

   
